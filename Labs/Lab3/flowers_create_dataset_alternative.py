"""
Categorizing the flower dataset
Creating the dataset
Author: Pierre Nugues
Source: https://github.com/pnugues/edan95/blob/master/labs/3.1-flowers_create_dataset.py
"""

import os
import random
import shutil
import numpy as np
from pathlib import Path

vilde = False
np.random.seed(0)

if vilde:
    base = Path('/home/pierre/')
else:
    base = Path('/Users/pierre/Documents/')

original_dataset_dir = base / 'Cours' / 'EDAN95/datasets/flowers'
dataset = os.path.join(base, 'Cours/EDAN95/datasets/flowers_split')

train_dir = os.path.join(dataset, 'train')
validation_dir = os.path.join(dataset, 'validation')
test_dir = os.path.join(dataset, 'test')

categories = os.listdir(original_dataset_dir)
categories = [category for category in categories if not category.startswith('.')]
print('Image types:', categories)
data_folders = [os.path.join(original_dataset_dir, category) for category in categories]


pairs = []
for folder, category in zip(data_folders, categories):
    images = os.listdir(folder)
    images = [image for image in images if not image.startswith('.')]
    pairs.extend([(image, category) for image in images])

random.shuffle(pairs)
img_nbr = len(pairs)
train_images = pairs[0:int(0.6 * img_nbr)]
val_images = pairs[int(0.6 * img_nbr):int(0.8 * img_nbr)]
test_images = pairs[int(0.8 * img_nbr):]

# print(train_images)
print(len(train_images))
print(len(val_images))
print(len(test_images))

for images_, dirname in [(train_images, train_dir), (val_images, validation_dir), (test_images, test_dir)]:
    for image, label in images_:
        src = os.path.join(original_dataset_dir, label, image)
        dst = os.path.join(dirname, label, image)
        os.makedirs(os.path.dirname(dst), exist_ok=True)
        shutil.copyfile(src, dst)
