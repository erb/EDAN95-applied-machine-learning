import os
import argparse
import random
import shutil
import numpy as np
from typing import Tuple, List
from pathlib import Path

from sklearn.metrics import confusion_matrix
from keras import layers
from keras import models
from keras import optimizers
from keras.utils import to_categorical
from keras.preprocessing.image import ImageDataGenerator


base_dir = '/home/felix/'
dataset_dir = os.path.join(base_dir, 'Documents/datasets/Flower_corpus/flowers_split')
original_dataset_dir = os.path.join(base_dir, 'Documents/datasets/Flower_corpus/flowers')


def print_when_run(f):
    print(f'Running {f}')
    return f


def _parse_args():
    parser = argparse.ArgumentParser(description="Program for predicting flower species")
    parser.add_argument('-t', '--train', action='store_true', help='Train model')
    parser.add_argument('-e', '--epochs', type=int, default=3, help='Number of epochs in training')
    parser.add_argument('-b', '--batch', type=int, default=5, help='Size of batches in training')
    parser.add_argument('--dataset_dir')
    parser.add_argument('--original_dataset_dir')
    return parser.parse_args()


def _restructure_dataset_dir(train_data, val_data, test_data):
    train_dir = os.path.join(dataset_dir, 'train')
    validation_dir = os.path.join(dataset_dir, 'validation')
    test_dir = os.path.join(dataset_dir, 'test')

    for dest_dir, data in ((train_dir, train_data), (validation_dir, val_data), (test_dir, test_data)):
        for image, label in data:
            src = os.path.join(original_dataset_dir, label, image)
            dst = os.path.join(dest_dir, label, image)
            os.makedirs(os.path.dirname(dst), exist_ok=True)
            shutil.copyfile(src, dst)


def _get_categories():
    categories = os.listdir(original_dataset_dir)
    categories = [category for category in categories if not category.startswith('.')]
    return categories


@print_when_run
def _load_data() -> Tuple[List, List, List]:
    categories = _get_categories()
    print('Image types:', categories)
    data_folders = [os.path.join(original_dataset_dir, category) for category in categories]

    pairs = []
    for folder, category in zip(data_folders, categories):
        images = os.listdir(folder)
        images = [image for image in images if not image.startswith('.')]
        pairs.extend([(image, category) for image in images])

    random.shuffle(pairs)
    img_nbr = len(pairs)
    train_data = pairs[0:int(0.6 * img_nbr)]
    val_data = pairs[int(0.6 * img_nbr):int(0.8 * img_nbr)]
    test_data = pairs[int(0.8 * img_nbr):]

    return train_data, val_data, test_data


def _print_dataset_info(train_data, val_data, test_data, batch_size):
    (train_images, train_labels) = zip(*train_data)
    (val_images, val_labels) = zip(*val_data)
    (test_images, test_labels) = zip(*test_data)

    print(type(train_images), type(train_labels))
    print('Amount of train images:', len(train_images))
    print('Amount of train labels:', len(train_labels))
    print('Amount of validation images:', len(val_images))
    print('Amount of validation labels:', len(val_labels))
    print('Amount of test images:', len(test_images))
    print('Amount of test labels:', len(test_labels))
    print('Batch size:', batch_size)


def _train_model():
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(150, 150, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Flatten())
    model.add(layers.Dense(512 * 2, activation='relu'))
    model.add(layers.Dense(5, activation='softmax'))
    model.summary()
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizers.Nadam(lr=1e-4),
                  metrics=['acc'])
    return model


def main():
    args = _parse_args()
    np.random.seed(0)

    trainModel = args.train
    epochs = args.epochs
    batch_size = args.batch

    if args.dataset_dir:
        global dataset_dir
        dataset_dir = args.dataset_dir
    if args.original_dataset_dir:
        global original_dataset_dir
        original_dataset_dir = args.original_dataset_dir

    train_dir = os.path.join(dataset_dir, 'train')
    validation_dir = os.path.join(dataset_dir, 'validation')
    test_dir = os.path.join(dataset_dir, 'test')

    train_data, val_data, test_data = _load_data()
    _print_dataset_info(train_data, val_data, test_data, batch_size)

    if not Path(dataset_dir).exists():
        _restructure_dataset_dir(train_data, val_data, test_data)

    # Compiling the model
    if trainModel:
        model = _train_model()

    # Creating datagenerators
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
    val_datagen = ImageDataGenerator(rescale=1. / 255)
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(150, 150),
        batch_size=batch_size,
        class_mode='categorical')

    validation_generator = val_datagen.flow_from_directory(
        validation_dir,
        target_size=(150, 150),
        batch_size=batch_size,
        class_mode='categorical')

    for data_batch, labels_batch in train_generator:
        print('Data batch shape:', data_batch.shape)
        print('Labels batch shape:', labels_batch.shape)
        print('')
        break

    # Training the model
    if trainModel:
        history = model.fit_generator(
            train_generator,
            steps_per_epoch=len(train_data) // batch_size,
            epochs=epochs,
            validation_data=validation_generator,
            validation_steps=len(val_data),
            verbose=2)

        model.save('flower_recognition_img_aug_1.h5')

        acc = history.history['acc']
        val_acc = history.history['val_acc']
        loss = history.history['loss']
        val_loss = history.history['val_loss']

        #print(acc)
        #print(val_acc)
        #print(loss)
        #print(val_loss)
    else:
        model = models.load_model('models/flower_recognition_img_aug_100_epochs.h5')

    # Testing the model
    test_generator = test_datagen.flow_from_directory(
        test_dir,
        target_size=(150, 150),
        batch_size=1,
        shuffle=False,
        class_mode='categorical')

    print('\nUsing model.evaluate_generator and printing the test loss and acc')
    test_loss, test_acc = model.evaluate_generator(test_generator, len(test_data), verbose=1)
    print('Test loss:', test_loss, '\tTest acc:', test_acc)

    predictions = model.predict_generator(test_generator, steps=test_generator.samples / test_generator.batch_size, verbose=1)
    predicted_classes = np.argmax(predictions, axis=1)

    import itertools
    labels = [k[1].argmax() for k in itertools.islice(test_generator, test_generator.samples)]
    cm = confusion_matrix(np.array(labels), np.array(predicted_classes))
    print(cm)


if __name__ == "__main__":
    main()
