import os
import argparse
import random
import shutil
import numpy as np
from sklearn.metrics import confusion_matrix
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator

parser = argparse.ArgumentParser(description="Program for predicting flower species")
parser.add_argument('-f', '--first', action='store_true', help='First run, will add directories')
parser.add_argument('-t', '--train', action='store_true', help='Train model')
parser.add_argument('-e', '--epochs', type=int, default=3, help='Number of epochs in training')
parser.add_argument('-b', '--batch', type=int, default=5, help='Size of batches in training')

args = parser.parse_args();

trainModel = args.train
firstRun = args.first
epochs = args.epochs
batch_size = args.batch


np.random.seed(0)

base = '/home/felix/'
original_dataset_dir = os.path.join(base, 'Documents/datasets/Flower_corpus/flowers')
dataset = os.path.join(base, 'Documents/datasets/Flower_corpus/flowers_split')

train_dir = os.path.join(dataset, 'train')
validation_dir = os.path.join(dataset, 'validation')
test_dir = os.path.join(dataset, 'test')

categories = os.listdir(original_dataset_dir)
categories = [category for category in categories if not category.startswith('.')]
print('Image types:', categories)
data_folders = [os.path.join(original_dataset_dir, category) for category in categories]


pairs = []
for folder, category in zip(data_folders, categories):
    images = os.listdir(folder)
    images = [image for image in images if not image.startswith('.')]
    pairs.extend([(image, category) for image in images])

random.shuffle(pairs)
img_nbr = len(pairs)
train_data = pairs[0:int(0.6 * img_nbr)]
val_data = pairs[int(0.6 * img_nbr):int(0.8 * img_nbr)]
test_data = pairs[int(0.8 * img_nbr):]

(train_images, train_labels) = zip(*train_data)
(val_images, val_labels) = zip(*val_data)
(test_images, test_labels) = zip(*test_data)


print(type(train_images), type(train_labels))
print('Amount of train images:', len(train_images))
print('Amount of train labels:', len(train_labels))
print('Amount of validation images:', len(val_images))
print('Amount of validation labels:', len(val_labels))
print('Amount of test images:', len(test_images))
print('Amount of test labels:', len(test_labels))
print('Batch size:', batch_size)

if firstRun:
    for image, label in train_data:
        src = os.path.join(original_dataset_dir, label, image)
        dst = os.path.join(train_dir, label, image)
        os.makedirs(os.path.dirname(dst), exist_ok=True)
        shutil.copyfile(src, dst)

    for image, label in val_data:
        src = os.path.join(original_dataset_dir, label, image)
        dst = os.path.join(validation_dir, label, image)
        os.makedirs(os.path.dirname(dst), exist_ok=True)
        shutil.copyfile(src, dst)

    for image, label in test_data:
        src = os.path.join(original_dataset_dir, label, image)
        dst = os.path.join(test_dir, label, image)
        os.makedirs(os.path.dirname(dst), exist_ok=True)
        shutil.copyfile(src, dst)

if trainModel:
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(150, 150, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Flatten())
    model.add(layers.Dense(512*2, activation='relu'))
    model.add(layers.Dense(5, activation='softmax'))

    model.summary()

    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizers.Nadam(lr=1e-4),
                  metrics=['acc'])

train_datagen = ImageDataGenerator(rescale=1./255)
val_datagen = ImageDataGenerator(rescale=1./255)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
    train_dir,
    target_size=(150, 150),
    batch_size=batch_size,
    class_mode='categorical')

validation_generator = val_datagen.flow_from_directory(
    validation_dir,
    target_size=(150, 150),
    batch_size=batch_size,
    class_mode='categorical')

for data_batch, labels_batch in train_generator:
    print('Data batch shape:', data_batch.shape)
    print('Labels batch shape:', labels_batch.shape)
    print('')
    break

if trainModel:
    history = model.fit_generator(
        train_generator,
        steps_per_epoch=len(train_data) // batch_size,
        epochs=epochs,
        validation_data=validation_generator,
        validation_steps=len(val_data),
        verbose=2)

    model.save('flower_recognition_1.h5')

    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    #print(acc)
    #print(val_acc)
    #print(loss)
    #print(val_loss)
else:
    model = models.load_model('flower_recognition_1.h5')

test_generator = test_datagen.flow_from_directory(
    test_dir,
    target_size=(150, 150),
    batch_size=1,
    shuffle=False,
    class_mode='categorical')

print('\nUsing model.evaluate_generator and printing the test loss and acc')
test_loss, test_acc = model.evaluate_generator(test_generator, len(test_data), verbose=1)
print('Test loss:', test_loss, '\tTest acc:', test_acc)

'''
test_generator.reset()
print('\nUsing model.predict_generator and printing the amount of correct guesses')
test_generator.reset()
counter = 0
for res, tr in zip(model.predict_generator(test_generator, verbose=1), test_labels):
    #print('Prediction:', categories[np.argmax(res)], '\tReal:', tr)
    if categories[np.argmax(res)] == tr:
        counter += 1
print(categories)
print('Correct guesses:', counter, 'out of', len(test_images))
'''




















