from typing import List
import pandas as pd
from keras.preprocessing.image import ImageDataGenerator
from pathlib import Path
import os

data_dir = './data/flowers'

files = [reversed(str(f).split("/")[-2:]) for d in Path(data_dir).iterdir() for f in d.iterdir() if f.is_file()]

df = pd.DataFrame(files, columns=['filename', 'label'])
dups = df[df.duplicated('filename')]


def move_duplicates(duplicates: pd.DataFrame) -> None:
    for i, (f, l) in duplicates.iterrows():
        print(f)
        os.rename(data_dir + "/" + l + "/" + f, data_dir + "/" + l + "/" + "dup_" + f)


print(dups)
move_duplicates(dups)

# df = df[~df['filename'].isin(duplicate_filenames)]

train_datagen = ImageDataGenerator()
train_datagen.flow_from_dataframe(df, './', has_ext=True, x_col='filename', y_col='label')
