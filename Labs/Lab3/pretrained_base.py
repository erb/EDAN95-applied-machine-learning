import os
import numpy as np
from pathlib import Path

from sklearn.metrics import confusion_matrix
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import VGG16
from keras import models
from keras import layers
from keras import optimizers
import matplotlib.pyplot as plt
from keras.utils import to_categorical
import joblib

memory = joblib.Memory('./.cache')


@memory.cache
def extract_features(directory, sample_count, aug=False):
    conv_base = VGG16(weights='imagenet',
                      include_top=False,
                      input_shape=(150, 150, 3))
    conv_base.summary()

    batch_size = 20
    if aug:
        datagen = ImageDataGenerator(
                rescale=1. / 255,
                rotation_range=40,
                width_shift_range=0.2,
                height_shift_range=0.2,
                shear_range=0.2,
                zoom_range=0.2,
                horizontal_flip=True)
    else:
        datagen = ImageDataGenerator(rescale=1. / 255)

    features = np.zeros(shape=(sample_count, 4, 4, 512))
    labels = np.zeros(shape=(sample_count, 5))
    generator = datagen.flow_from_directory(
        directory,
        target_size=(150, 150),
        batch_size=batch_size,
        class_mode='categorical')
    i = 0
    for inputs_batch, labels_batch in generator:
        features_batch = conv_base.predict(inputs_batch)
        features[i * batch_size: (i + 1) * batch_size] = features_batch
        labels[i * batch_size: (i + 1) * batch_size] = labels_batch
        i += 1
        if i * batch_size >= sample_count:
            break
    return features, labels


def main():
    import sys
    dataset_dir = sys.argv.pop()

    train_dir = os.path.join(dataset_dir, 'train')
    validation_dir = os.path.join(dataset_dir, 'validation')
    test_dir = os.path.join(dataset_dir, 'test')

    train_dir_len = 2595
    validation_dir_len = 865
    test_dir_len = 866


    print('Extracting features...')

    train_features, train_labels = extract_features(train_dir, train_dir_len, aug=True)
    validation_features, validation_labels = extract_features(validation_dir, validation_dir_len)
    test_features, test_labels = extract_features(test_dir, test_dir_len)

    print('Extracted features')

    train_features = np.reshape(train_features, (train_dir_len, 4 * 4 * 512))
    validation_features = np.reshape(validation_features, (validation_dir_len, 4 * 4 * 512))
    test_features = np.reshape(test_features, (test_dir_len, 4 * 4 * 512))

    print(train_features.shape)
    print(train_labels.shape)

    print(validation_features.shape)
    print(validation_labels.shape)

    model_path = 'pretrained_base.h5'
    if Path(model_path).exists():
        model = models.load_model(model_path)

    if 'train' in sys.argv:
        model = models.Sequential()
        model.add(layers.Dense(256, activation='relu', input_dim=4 * 4 * 512))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(5, activation='softmax'))
        model.compile(optimizer=optimizers.RMSprop(lr=2e-5),
                      loss='categorical_crossentropy',
                      metrics=['acc'])
        history = model.fit(train_features, train_labels,
                            epochs=10,
                            batch_size=20,
                            validation_data=(validation_features, validation_labels))
        model.save('pretrained_base.h5')

    _, test_acc = model.evaluate(test_features, test_labels)
    print(test_acc)

    # predictions = model.predict_generator(test_generator, steps=800, verbose=1)
    predictions = model.predict(test_features)
    print(predictions)
    predicted_classes = np.argmax(predictions, axis=1)

    import itertools
    labels = np.argmax(test_labels, axis=1)
    cm = confusion_matrix(np.array(labels), np.array(predicted_classes))
    print(cm)


    # print(history.history.values())


if __name__ == "__main__":
    main()
