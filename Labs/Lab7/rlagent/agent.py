"""agent.py: Contains the entire deep reinforcement learning agent."""
__author__ = "Erik Gärtner"

from collections import deque

import tensorflow as tf
import numpy as np
import math

from .expreplay import ExpReplay


class Agent():
    """
    The agent class where you should implement the vanilla policy gradient agent.
    """

    def __init__(self, tf_session, state_size=(4,), action_size=2,
                 learning_rate=1e-3, gamma=0.99, memory_size=5000):
        """
        The initialization function. Besides saving attributes we also need
        to create the policy network in Tensorflow that later will be used.
        """

        self.state_size = state_size
        self.action_size = action_size
        self.tf_session = tf_session
        self.gamma = gamma
        self.replay = ExpReplay(memory_size)

        with tf.variable_scope('agent'):
            # Create tf placeholders, i.e. inputs into the network graph.
            self.inputs = tf.placeholder(tf.float32, shape=[None, state_size[0]])

            print(self.inputs)
            # Create the hidden layers
            hidden = tf.layers.dense(self.inputs, 30, activation=tf.nn.relu)
            hidden = tf.layers.dense(hidden, self.action_size, activation=None)

            # Create the loss. We need to multiply the reward with the
            # log-probability of the selected actions.
            # def loss_fun(rewards, training_batch):
                # L = -\sum_i A_i\log(\pi_\theta(a_i | s_i))
                # return -sum(rewards[i] * math.log(action_probability(action[i], state[i])) for i in range(len(training_batch)))

            self._output = tf.nn.softmax(hidden)

            log_prob = tf.log(self._output)
            self._acts = tf.placeholder(tf.int32)
            self._advantages = tf.placeholder(tf.float32)

            indices = tf.range(0, tf.shape(log_prob)[0]) * tf.shape(log_prob)[1] + self._acts
            act_prob = tf.gather(tf.reshape(log_prob, [-1]), indices)

            loss = -tf.reduce_sum(tf.multiply(act_prob, self._advantages))
            # loss = tf.log(tf.nn.softmax(nn))
            # loss = tf.reduce_mean(loss)
            # print(loss)
            # loss = tf.nn.softmax_cross_entropy_with_logits(logits=output, labels=tf.onehot(self._acts))

            # Create the optimizer to minimize the loss
            self._train = tf.train.AdamOptimizer(learning_rate).minimize(loss)

        tf_session.run(tf.global_variables_initializer())

    def take_action(self, state):
        """
        Given the current state sample an action from the policy network.
        Return a the index of the action [0..N).
        """
        action = self.tf_session.run(self._output, feed_dict={self.inputs: [state]})
        action = np.random.choice(range(self.action_size), p=action[0])
        return action

    def record_action(self, state0, action, reward, state1, done):
        """
        Record an action taken by the action and the associated reward
        and next state. This will later be used for traning.
        """
        self.replay.add({'state0': state0, 'action': action, 'reward': reward, 'state1': state1})

    def train_agent(self):
        """
        Train the policy network using the collected experiences during the
        episode(s).
        """
        # Retrieve collected experiences from memory
        exps = self.replay.get_all()
        states = [exp['state0'] for exp in exps]
        actions = [exp['action'] for exp in exps]
        rewards = [exp['reward'] for exp in exps]

        # Discount and normalize rewards
        discounted_rewards = self.discount_rewards_and_normalize(rewards)

        # Shuffle for better learning

        # Feed the experiences through the network with rewards to compute and
        # minimize the loss.
        self.tf_session.run(self._train, feed_dict={
            self.inputs: states,
            self._acts: actions,
            self._advantages: discounted_rewards
        })

        self.replay.clear()

    def discount_rewards_and_normalize(self, rewards):
        """
        Given the rewards for an epsiode discount them by gamma.
        Next since we are sending them into the neural network they should
        have a zero mean and unit variance.

        Return the new list of discounted and normalized rewards.
        """
        dr = np.cumsum([self.gamma**k * r
                        for k, r in enumerate(rewards)][::-1])[::-1]
        dr = (dr - dr.mean()) / dr.std()
        return dr
