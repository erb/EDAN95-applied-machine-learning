import argparse
import sys
import os
from sklearn.feature_extraction import DictVectorizer
import time
from keras import models, layers, callbacks
import sys
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras.models import load_model
import math
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import LSTM, Bidirectional, SimpleRNN, Dense
import re


def _parse_args():
    parser = argparse.ArgumentParser(description="Main")
    parser.add_argument('-t', '--train', action='store_true', help='Train model')
    return parser.parse_args()


def main():
    args = _parse_args()
    OPTIMIZER = 'rmsprop'
    BATCH_SIZE = 128
    EPOCHS = 2
    EMBEDDING_DIM = 100
    MAX_SEQUENCE_LENGTH = 150
    LSTM_UNITS = 512


    def load(file):
        """
        Return the embeddings in the from of a dictionary
        :param file:
        :return:
        """
        file = file
        embeddings = {}
        glove = open(file)
        for line in glove:
            values = line.strip().split()
            word = values[0]
            vector = np.array(values[1:], dtype='float32')
            embeddings[word] = vector
        glove.close()
        embeddings_dict = embeddings
        embedded_words = sorted(list(embeddings_dict.keys()))
        return embeddings_dict


    embedding_file = '../../../data/Glove_6B/glove.6B.100d.txt'
    embeddings_dict = load(embedding_file)


    def load_conll2003():

        train_file = '../../../data/NER-data/eng.train'
        dev_file = '../../../data/NER-data/eng.valid'
        test_file = '../../../data/NER-data/eng.test'
        column_names = ['word', 'pos', 'tag', 'entity']
        column_names = list(map(str.lower, column_names))
        train_sentences = open(train_file).read().strip()
        dev_sentences = open(dev_file).read().strip()
        test_sentences = open(test_file).read().strip()
        # test2_sentences = open(test2_file).read().strip()
        return train_sentences, dev_sentences, test_sentences, column_names

    train_sentences, dev_sentences, test_sentences, column_names = load_conll2003()
    # print(train_sentences[:100])
    # print()


    class Token(dict):
        pass

    class CoNLLDictorizer:

        def __init__(self, column_names, sent_sep='\n\n', col_sep=' '):
            self.column_names = column_names
            self.sent_sep = sent_sep
            self.col_sep = col_sep

        def fit(self):
            pass

        def transform(self, corpus):
            corpus = corpus.strip()
            sentences = re.split(self.sent_sep, corpus)
            return list(map(self._split_in_words, sentences))

        def fit_transform(self, corpus):
            return self.transform(corpus)

        def _split_in_words(self, sentence):
            rows = re.split('\n', sentence)
            rows = [row for row in rows if row[0] != '#']
            return [Token(dict(zip(self.column_names,
                                   re.split(self.col_sep, row))))
                    for row in rows]


    conll_dict = CoNLLDictorizer(column_names)
    train_dict = conll_dict.transform(train_sentences)
    dev_dict = conll_dict.transform(dev_sentences)
    test_dict = conll_dict.transform(test_sentences)
    # print('First sentence, train:', train_dict[1])
    # print()


    def build_sequences(corpus_dict, key_x='word', key_y='pos', tolower=True):
        """
        Creates sequences from a list of dictionaries
        :param corpus_dict:
        :param key_x:
        :param key_y:
        :return:
        """
        X = []
        Y = []
        for sentence in corpus_dict:
            x = []
            y = []
            for word in sentence:
                x += [word[key_x]]
                y += [word[key_y]]
            if tolower:
                x = list(map(str.lower, x))
            X += [x]
            Y += [y]
        return X, Y

    X_train_cat, Y_train_cat = build_sequences(train_dict)
    # print('First sentence, words', X_train_cat[1])
    # print('First sentence, POS', Y_train_cat[1])
    # print()

    vocabulary_words = sorted(list(set([word for sentence in X_train_cat for word in sentence])))
    pos = sorted(list(set([pos for sentence in Y_train_cat for pos in sentence])))
    # print(pos)
    NB_CLASSES = len(pos)
    # print()

    embeddings_words = embeddings_dict.keys()
    # print('Words in GloVe:', len(embeddings_dict.keys()))
    vocabulary_words = sorted(list(set(vocabulary_words + list(embeddings_words))))
    cnt_uniq = len(vocabulary_words) + 2
    # print('# unique words in the vocabulary: embeddings and corpus:', cnt_uniq)
    # print()


    def to_index(X, idx):
        """
        Convert the word lists (or POS lists) to indexes
        :param X: List of word (or POS) lists
        :param idx: word to number dictionary
        :return:
        """
        X_idx = []
        for x in X:
            # We map the unknown words to one
            x_idx = list(map(lambda x: idx.get(x, 1), x))
            X_idx += [x_idx]
        return X_idx

    # We start at one to make provision for the padding symbol 0
    # in RNN and LSTMs and 1 for the unknown words
    rev_word_idx = dict(enumerate(vocabulary_words, start=2))
    rev_pos_idx = dict(enumerate(pos, start=2))
    word_idx = {v: k for k, v in rev_word_idx.items()}
    pos_idx = {v: k for k, v in rev_pos_idx.items()}
    # print('word index:', list(word_idx.items())[:10])
    # print('POS index:', list(pos_idx.items())[:10])
    # print()

    # We create the parallel sequences of indexes
    X_idx = to_index(X_train_cat, word_idx)
    Y_idx = to_index(Y_train_cat, pos_idx)
    # print('First sentences, word indices', X_idx[:3])
    # print('First sentences, POS indices', Y_idx[:3])
    # print()

    X = pad_sequences(X_idx)
    Y = pad_sequences(Y_idx)

    # print(X[1])
    # print(Y[1])

    # The number of POS classes and 0 (padding symbol)
    Y_train = to_categorical(Y, num_classes=len(pos) + 2)
    # print(Y_train[1])
    # print()

    rdstate = np.random.RandomState(1234567)
    embedding_matrix = rdstate.uniform(-0.05, 0.05, (len(vocabulary_words) + 2, EMBEDDING_DIM))

    for word in vocabulary_words:
        if word in embeddings_dict:
            # If the words are in the embeddings, we fill them with a value
            embedding_matrix[word_idx[word]] = embeddings_dict[word]

    # print('Shape of embedding matrix:', embedding_matrix.shape)
    # print('Embedding of table', embedding_matrix[word_idx['table']])
    # print('Embedding of the padding symbol, idx 0, random numbers', embedding_matrix[0])
    # print()

    model = models.Sequential()

    def train_new_model(model, verbose=0):
        model.add(layers.Embedding(len(vocabulary_words) + 2,
                                   EMBEDDING_DIM,
                                   mask_zero=True,
                                   input_length=None))
        model.layers[0].set_weights([embedding_matrix])
        # The default is True
        model.layers[0].trainable = True
        # model.add(SimpleRNN(100, return_sequences=True))
        # model.add(Bidirectional(SimpleRNN(100, return_sequences=True)))
        # model.add(layers.Dropout(0.5))
        model.add(Bidirectional(LSTM(100, return_sequences=True)))
        model.add(layers.Dropout(0.5))
        model.add(Dense(NB_CLASSES + 2, activation='softmax'))
        '''
        callbacks_list = [
            callbacks.EarlyStopping(
                monitor='acc',
                patience=1),
            callbacks.ModelCheckpoint(
                filepath='model.h5',
                monitor='val_loss',
                save_best_only=True)
        ]
        '''
        model.compile(loss='categorical_crossentropy',
                      optimizer=OPTIMIZER,
                      metrics=['acc'])
        model.fit(X, Y_train, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=verbose)
        model.summary()
        model.save('model.h5')
        return model

    if args.train:
        model = train_new_model(model, 2)
    else:
        try:
            model = models.load_model('model.h5')
        except OSError:
            model = train_new_model(model, 2)

    # In X_dict, we replace the words with their index
    X_test_cat, Y_test_cat = build_sequences(test_dict)
    # We create the parallel sequences of indexes
    X_test_idx = to_index(X_test_cat, word_idx)
    Y_test_idx = to_index(Y_test_cat, pos_idx)

    # print('X[0] test idx', X_test_idx[0])
    # print('Y[0] test idx', Y_test_idx[0])

    X_test_padded = pad_sequences(X_test_idx)
    Y_test_padded = pad_sequences(Y_test_idx)
    # print('X[0] test idx passed', X_test_padded[0])
    # print('Y[0] test idx padded', Y_test_padded[0])
    # print()
    # One extra symbol for 0 (padding)
    Y_test_padded_vectorized = to_categorical(Y_test_padded,
                                              num_classes=len(pos) + 2)
    # print('Y[0] test idx padded vectorized', Y_test_padded_vectorized[0])
    # print(X_test_padded.shape)
    # print(Y_test_padded_vectorized.shape)
    # print()

    # Evaluates with the padding symbol
    test_loss, test_acc = model.evaluate(X_test_padded,
                                         Y_test_padded_vectorized)
    # print('Loss:', test_loss)
    # print('Accuracy:', test_acc)
    # print()

    # print('X_test', X_test_cat[1])
    # print('X_test_padded', X_test_padded[1])
    corpus_pos_predictions = model.predict(X_test_padded)
    # print()
    # print('Y_test', Y_test_cat[1])
    # print('Y_test_padded', Y_test_padded[1])
    # print('predictions', corpus_pos_predictions[1])
    # print()

    pos_pred_num = []
    for sent_nbr, sent_pos_predictions in enumerate(corpus_pos_predictions):
        pos_pred_num += [sent_pos_predictions[-len(X_test_cat[sent_nbr]):]]
    # print(pos_pred_num[:2])
    # print()

    pos_pred = []
    for sentence in pos_pred_num:
        pos_pred_idx = list(map(np.argmax, sentence))
        pos_pred_cat = list(map(rev_pos_idx.get, pos_pred_idx))
        pos_pred += [pos_pred_cat]

    # print(pos_pred[:2])
    # print(Y_test_cat[:2])

    total, correct, total_ukn, correct_ukn = 0, 0, 0, 0
    for id_s, sentence in enumerate(X_test_cat):
        for id_w, word in enumerate(sentence):
            total += 1
            if pos_pred[id_s][id_w] == Y_test_cat[id_s][id_w]:
                correct += 1
            # The word is not in the dictionary
            if word not in word_idx:
                total_ukn += 1
                if pos_pred[id_s][id_w] == Y_test_cat[id_s][id_w]:
                    correct_ukn += 1

    print('total %d, correct %d, accuracy %f' % (total, correct, correct / total))
    if total_ukn != 0:
        print('total unknown %d, correct %d, accuracy %f' % (total_ukn, correct_ukn, correct_ukn / total_ukn))

    def predict_sentence(sentence, model, word_idx,
                         vocabulary_words, rev_idx_pos, verbose=False):
        # Predict one sentence
        sentence = sentence.split()
        word_idxs = to_index([sentence], word_idx)
        word_idx_padded = pad_sequences(word_idxs)

        pos_idx_pred = model.predict(word_idx_padded)
        # We remove padding
        pos_idx_pred = pos_idx_pred[0][-len(sentence):]
        pos_idx = list(map(np.argmax, pos_idx_pred))
        pos = list(map(rev_idx_pos.get, pos_idx))
        if verbose:
            print('Sentence', sentence)
            print('Sentence word indexes', word_idxs)
            print('Padded sentence', word_idx_padded)
            print('POS predicted', pos_idx_pred[0])
            print('POS shape', pos_idx_pred.shape)
        return pos


    sentences = ["That round table might collapse .",
                 "The man can learn well .",
                 "The man can swim .",
                 "The man can simwo ."]
    for sentence in sentences:
        y_test_pred_cat = predict_sentence(sentence.lower(),
                                           model, word_idx,
                                           vocabulary_words,
                                           rev_pos_idx)
        print(sentence)
        print(y_test_pred_cat)


if __name__ == '__main__':
    main()
