import numpy as np
import joblib
from typing import Dict, List, Tuple

memory = joblib.Memory('./.cache')


def create_inverted_indices(forward_list: List[str]) -> Dict[str, int]:
    print('Creating inverted indices...')
    inverted_dict = dict()
    for i in range(len(forward_list)):
        inverted_dict[forward_list[i]] = i
    print('Inverted indices created.')
    return inverted_dict


@memory.cache
def load_glove_model(path: str) -> Dict[str, np.array]:
    print("Loading Glove Model...")
    f = open(path, 'r')
    model = {}
    for line in f:
        split_line = line.split()
        word = split_line[0]
        embedding = np.array([float(val) for val in split_line[1:]])
        model[word] = embedding
    print("Loaded glove model,", len(model), " words loaded.")
    return model


@memory.cache
def extract_ner(path: str) -> Tuple[List[str], List[str]]:
    print('Extracting NER...')
    x_list = ['PAD', 'UNKNOWN']
    y_list = ['PAD', 'UNKNOWN']
    with open(path, 'r') as f:
        for line in f:
            words = line.split()
            x_list.append(' '.join(words[1:]).lower())  # word
            y_list.append(words[0])  # NER tag of word
    print('Extracted', len(x_list), 'words with', len(y_list), 'tags.')
    return x_list, y_list


@memory.cache
def extract_sentences(path: str) -> List[List[str]]:
    with open(path, 'r') as f:
        lines = f.readlines()[2:]
        sentences = []
        curr_sentence: List[str] = []
        for line in lines:
            if line.strip():
                curr_sentence.append(line.split(" ")[0].lower())
            else:
                sentences.append(curr_sentence)
                curr_sentence = []
    return sentences
