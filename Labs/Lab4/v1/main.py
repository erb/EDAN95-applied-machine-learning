import argparse
import numpy as np
import sys
import joblib

import keras
from keras import layers
from keras import models
from keras import optimizers

from typing import List, Dict, Set  # , Any

import CoNLLDictorizer as CoNLL
import extracter as ex

memory = joblib.Memory('./.cache')

Glove = Dict[str, np.array]


def _parse_args():
    parser = argparse.ArgumentParser(description="Main")
    parser.add_argument('--dataset_dir')
    parser.add_argument('--original_dataset_dir')
    return parser.parse_args()


#@memory.cache
def five_closest_words(words: List[str], glove_mod: Glove):
    words_pos = dict()

    all_results = list()
    for word in words:
        word = word.lower()
        curr_results = list([('init', 100000), ('init2', 200000), ('init3', 300000), ('init4', 40000), ('init5', 50000)])

        words_pos[word] = glove_mod[word]

        for contender in glove_mod:
            dist = np.linalg.norm(words_pos[word] - glove_mod[contender])
            for i in range(len(curr_results)):
                if dist < curr_results[i][1] and word != contender:
                    curr_results.insert(i, (contender, dist))
                    curr_results.pop()
                    break
        all_results.append(curr_results)
    return all_results


# @memory.cache
def create_vocabulary(path: str, glove_mod: Glove) -> List[str]:
    vocab: Set[str] = set()

    with open(path, 'r') as f:
        lines = f.readlines()[1:]
        vocab |= set([line.split()[0].lower() for line in lines if len(line.split()) > 0])

    vocab |= set(glove_mod.keys())
    return sorted(list(vocab))


# @memory.cache
def build_embedding_matrix(vocab: List[str], glove_mod: Glove) -> np.ndarray:
    rdstate = np.random.RandomState(1234567)
    emb_mat = rdstate.uniform(-0.05, 0.05,
                              (len(vocab) + 2,
                              EMBEDDING_DIM))
    for i in range(len(vocab)):
        if vocab[i] in glove_mod:
            emb_mat[i + 2] = glove_mod[vocab[i]]
    return emb_mat


def build_sequences(corpus_dict, key_x='form', key_y='upos', tolower=True):
    """
    Creates sequences from a list of dictionaries
    :param corpus_dict:
    :param key_x:
    :param key_y:
    :return:
    """
    X = []
    Y = []
    for sentence in corpus_dict:
        x = []
        y = []
        for word in sentence:
            x += [word[key_x]]
            y += [word[key_y]]
        if tolower:
            x = list(map(str.lower, x))
        X += [x]
        Y += [y]
    return X, Y


if __name__ == '__main__':
    #def main() -> None:
    OPTIMIZER = 'rmsprop'
    BATCH_SIZE = 128
    EPOCHS = 2
    EMBEDDING_DIM = 100
    MAX_SEQUENCE_LENGTH = 150
    LSTM_UNITS = 512

    glove_model = ex.load_glove_model('../../data/Glove_6B/glove.6B.100d.txt')

    x_list, y_list = ex.extract_ner('../../data/NER-data/eng.list')

    # def convert_to_numbered(l: List[str], inv_indices=None) -> List[int]:
    #     if inv_indices is None:
    #         inv_indices = ex.create_inverted_indices(l)
    #     return [inv_indices[word] for word in l]

    def convert_to_numbered(l, inv_voc):
        inv_indices = []
        for i in range(len(l)):
            inv_indices.append([inv_voc[word] if word in inv_voc else 1 for word in l[i]])
        return inv_indices


    # x_list_idx = convert_to_numbered(x_list)
    # print(x_list_idx[:10])
    x_list_reverse_index = ex.create_inverted_indices(x_list)

    # print('Finding closest words...')
    # five_closest = five_closest_words(['table', 'france', 'sweden'], glove_model)
    # print('Found closest words.')
    # for res in five_closest:
    #     print(res)

    print('Creating vobaculary...')
    vocabulary = create_vocabulary('../../../data/NER-data/eng.train', glove_model)
    print('Created vobaculary.')

    print('Creating inverted vobaculary...')
    inverted_vocabulary = ex.create_inverted_indices(vocabulary)
    print('Created inverted vobaculary.')

    print('Creating embedded matrix...')
    embedded_matrix = build_embedding_matrix(vocabulary, glove_model)
    # print(embedded_matrix[:10])
    print('Created embedded matrix.')

    sentences = ex.extract_sentences('../../../data/NER-data/eng.train')
    sentences_nbrd = convert_to_numbered(sentences, inverted_vocabulary)
    train_sentences = keras.preprocessing.sequence.pad_sequences(sentences_nbrd)

    val_sentences = ex.extract_sentences('../../../data/NER-data/eng.valid')
    val_sentences_nbrd = convert_to_numbered(val_sentences, inverted_vocabulary)
    val_sentences = keras.preprocessing.sequence.pad_sequences(val_sentences_nbrd)

    # print(train_sentences[:5])
    # print(valid_sentences[:5])

    pos = sorted(list(set([pos for sentence
                           in y_list for pos in sentence])))

    column_names = ['id', 'POS', 'Something1', 'Something2']
    conll_dict = CoNLL.CoNLLDictorizer(column_names, col_sep=' ')
    train_dict = conll_dict.transform(train_sentences)
    print('First sentence, train:', train_dict[0])
    '''
    model = models.Sequential()
    model.add(layers.Embedding(len(vocabulary) + 2,
                               EMBEDDING_DIM,
                               mask_zero=True,
                               input_length=None))
    model.layers[0].set_weights([embedded_matrix])
    # The default is True
    model.layers[0].trainable = True
    model.add(layers.SimpleRNN(100, return_sequences=True))
    # model.add(Bidirectional(SimpleRNN(100, return_sequences=True)))
    # model.add(Bidirectional(LSTM(100, return_sequences=True)))
    model.add(layers.Dense(len(pos) + 2, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['acc'])
    model.summary()
    model.fit(x_list, y_list, epochs=EPOCHS, batch_size=BATCH_SIZE)
    '''
#if __name__ == '__main__':
#    main()
