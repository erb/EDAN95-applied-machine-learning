.PHONY: test

test:
	pipenv run python3 -m mypy --ignore-missing-imports */*.py
	pipenv run python3 -m pytest test/


jupyter:
	# From: https://stackoverflow.com/a/47296960/965332
	pipenv run bash -c 'python -m ipykernel install --user --name=`basename $$VIRTUAL_ENV`'
